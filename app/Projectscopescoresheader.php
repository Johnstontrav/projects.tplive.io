<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projectscopescoresheader extends Model
{


    public function getProjectstageAttribute($value)
    {
        return ucwords($value);
    }


}
