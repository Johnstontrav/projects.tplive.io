<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Projectscopescoresuser extends Model
{

    public function comments()
    {
        return $this->hasMany('App\Projectscopefeedback','projectscope_id', 'id')->with('user')->orderBy('created_at','DESC');
    }

    public function getPassfailAttribute($value)
    {
        if ($value ==1)
            return true;

        return false;
    }

    /**
     * @return string
     */
    public function getCreatedAtAttribute()
    {
        return  Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    /**
     * @return string
     */
    public function getUpdatedAtAttribute()
    {
        return  Carbon::parse($this->attributes['updated_at'])->diffForHumans();
    }


    public function getProjectstageAttribute($value)
    {
       return ucwords($value);
    }

    public function getFunctiongroupAttribute($value)
    {
       return ucwords($value);
    }

    public function getFunctionAttribute($value)
    {
       return ucfirst($value);
    }


}
