<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projectscopescoresheaderuser extends Model
{


    public function getProjectstageAttribute($value)
    {
        return ucwords($value);
    }


}
