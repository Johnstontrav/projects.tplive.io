<?php

namespace App\Providers;

use App\Company;
use App\Repository\UserRepository;
use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Auth0\Login\Contract\Auth0UserRepository as Auth0UserRepositoryContract;
use Auth0\Login\Repository\Auth0UserRepository as Auth0UserRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        \View::composer('*', function($view){

            $companies = Company::get();
            $user = User::with('company')->find(\Auth::id());

            $view->with('companies', $companies);
            $view->with('user', $user);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Auth0\Login\Contract\Auth0UserRepository',
            UserRepository::class);
    }
}
