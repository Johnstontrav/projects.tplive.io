<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class AuthCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::with('company')->find(\Auth::id());

        if ($request->route('project') != $user->company->name)
            return redirect('/home');

        return $next($request);
    }
}
