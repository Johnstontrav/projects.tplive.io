<?php

namespace App\Http\Controllers;

use App\Projectscope;
use App\Projectscopefeedback;
use App\Projectscopescore;
use App\Projectscopescoresheader;
use App\Projectscopescoresheaderuser;
use App\Projectscopescoresuser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JavaScript;

class Projectscopes extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('projectscope.index');
    }

    public function combos(Request $request)
    {
        $projectname = $request->route('project');

        $combos = [];
        $combos['projectstagegroups'] = Projectscope::where('projectname', $projectname)->distinct()->get(['projectstagegroups'])->pluck('projectstagegroups');
        $combos['projectstage'] = Projectscope::where('projectname', $projectname)->distinct()->get(['projectstage'])->pluck('projectstage');
        $combos['functiongroup'] = Projectscope::where('projectname', $projectname)->distinct()->get(['functiongroup'])->pluck('functiongroup');


        return $combos;
    }

    public function get($user_id, Request $request)
    {

        $projectname = $request->route('project');
        $user = User::find(Auth::id());

        $feedback = null;

        if ($user_id == 0) {

            $data = Projectscopescore::with('comments')->where('projectname', $projectname)->orderBy('projectstagegroupssorter', 'ASC')->orderBy('projectstage', 'ASC')->orderBy('functiongroup', 'ASC')->get();

            $totals = Projectscopescoresheader::where('projectname', $projectname)->get()->groupBy('projectstage')->toArray();

        } else {

            $totals = Projectscopescoresheaderuser::where('user_id', $user_id)->where('projectname', $projectname)->get()->groupBy('projectstage')->toArray();

            $data = Projectscopescoresuser::with('comments')->where('projectname', $projectname)->where('user_id', $user_id)->orderBy('projectstagegroupssorter', 'ASC')->orderBy('projectstage', 'ASC')->orderBy('functiongroup', 'ASC')->get();

        }

        $grouped = [];

        foreach ($data as $item) {

            $key1 = $item->projectstagegroups;
            $key2 = $item->projectstage;
            $key3 = $item->functiongroup;

            if (!isset($grouped[$key1]['items'][$key2]['items'][$key3]['items'])) {
                $grouped[$key1]['name'] = $key1;
                $grouped[$key1]['items'][$key2]['name'] = $key2;
                $grouped[$key1]['items'][$key2]['show'] = false;
                $grouped[$key1]['items'][$key2]['client_edit'] = false;
                if ($key2 == 'A) Requirements - Client')
                    $grouped[$key1]['items'][$key2]['client_edit'] = true;

                if ($user->company->name == 'Touchpoint')
                    $grouped[$key1]['items'][$key2]['client_edit'] = true;

                $grouped[$key1]['items'][$key2]['totals'] = ['Pass' => 0, 'Fail' => 0, 'Responses' => 0, 'HexColor' => '#43ac6a', 'SuccessRate' => 0];
                if (isset($totals[$key2])) {

                    $grouped[$key1]['items'][$key2]['totals'] = array_first($totals[$key2]);
                }
                $grouped[$key1]['items'][$key2]['items'][$key3]['name'] = $key3;
                $grouped[$key1]['items'][$key2]['items'][$key3]['items'] = [];
            }

            $item->sending = false;
            $item->dirty = false;
            $item->details = false;

            $grouped[$key1]['items'][$key2]['items'][$key3]['items'][] = $item;

        }

        return $grouped;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projectscope.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Projectscope();
        $item->projectname = $request->route('project');
        $item->projectstagegroups = $request->input('projectstagegroups');
        $item->projectstage = $request->input('projectstage');
        $item->functiongroup = $request->input('functiongroup');
        $item->projectstagegroupssorter = $request->input('projectstagegroupssorter');
        $item->function = $request->input('function');
        $item->save();

        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($project, $id)
    {

        $status = Projectscope::find($id)->delete();

        return ['status' => $status];
    }
}
