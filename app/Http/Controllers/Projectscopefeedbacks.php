<?php

namespace App\Http\Controllers;

use App\Projectscope;
use App\Projectscopefeedback;
use App\Projectscopescore;
use App\Projectscopescoresheader;
use App\Projectscopescoresheaderuser;
use App\Projectscopescoresuser;
use Auth;
use Illuminate\Http\Request;
use JavaScript;

class Projectscopefeedbacks extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $item = new Projectscopefeedback();
        $item->projectscope_id = $request->input('projectscope_id');
        $item->user_id = Auth::id();
        $item->passfail = $request->input('passfail');
        $item->comment = $request->input('comment');

        $item->save();

        Projectscope::find($item->projectscope_id)->update(['function' => $request->input('func'),'status' => $request->input('status')]);

        if ($request->input('user') == 0) {

            $item = Projectscopescore::with('comments')->find($request->input('projectscope_id'));

            $item->totals = Projectscopescoresheader::where('projectstage', $item->projectstage)->first();
        }else {


            $item = Projectscopescoresuser::with('comments')->where('user_id', $request->input('user'))->find($request->input('projectscope_id'));

            $item->totals = Projectscopescoresheaderuser::where('user_id', $request->input('user'))->where('projectstage', $item->projectstage)->first();

        }

         return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
