<?php

namespace App\Http\Controllers;

use Auth0\SDK\Auth0;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        $isLoggedIn = \Auth::check();

        return view('index')
            ->with('isLoggedIn', $isLoggedIn)
            ->with('user', \Auth::user());
    }

    public function login()
    {
        return \App::make('auth0')->login(null,null,['scope' => 'openid profile email']);
    }

    public function logout()
    {
        \Auth::logout();
        return \Redirect::intended('/');
    }
}
