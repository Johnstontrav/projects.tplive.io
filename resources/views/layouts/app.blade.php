<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', config('app.name'))</title>

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link href='//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet">
    <link href="//gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">


    <!-- Global Spark Object -->
    <script>
        @if (isset($js))
		window.payload = <?php echo json_encode($js); ?>;
		@endif
        @if (isset($users))
		window.users = <?php echo json_encode($users); ?>;
		@endif
    </script>

</head>
<body class="with-navbar">

    <div id="app" v-cloak>
        <!-- Navigation -->
        @include('nav.user')

        <!-- Main Content -->
        @yield('content')

    </div>

    <!-- JavaScript -->

    <script src="{{ mix('js/app.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    @if(session()->has('flash_message'))
        <script type="text/javascript">
		    swal({
			    title: "{!! session('flash_message.title') !!}",
			    text: "{!! session('flash_message.message') !!}",
			    type: "{!! session('flash_message.level') !!}",
                @if(session('flash_message.timer')) timer: "{!! session('flash_message.timer') !!}" @endif
		    });
        </script>
    @endif


    <script src="//gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script>
	    $('form').submit(function (event) {
		    if ($(this).hasClass('submitted')) {
			    event.preventDefault();
		    }
		    else {
			    $(this).find(':submit').html('<i class="fa fa-spinner fa-spin"></i>');
			    $(this).addClass('submitted');
		    }
	    });
    </script>

    @include ('footer')

    <!-- Scripts -->
    @yield('scripts', '')

</body>
</html>
