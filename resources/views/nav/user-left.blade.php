@if (strtolower($user->company->name) == 'touchpoint')
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
           aria-expanded="false">Setup <span class="caret"></span></a>
        <ul class="dropdown-menu">
            @foreach($companies as $company)
            <li><a href="/{{$company->name}}/projectscope">{{$company->name}} - Scope</a></li>
                @endforeach
        </ul>
    </li>
@else
    <li>
        <a href="/{{$user->company->name}}/projectscope">Project Scope</a>
    </li>
@endif