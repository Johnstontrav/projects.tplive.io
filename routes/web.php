<?php


use Auth0\Login\facade\Auth0;

Route::get('/', function () {

    if (Auth0::getUser()) {
        return redirect('/home');
    }
    return view('welcome');

});


Route::get('/login', ['as' => 'login', 'uses' => 'IndexController@login']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'IndexController@logout']);
Route::get('/callback', '\Auth0\Login\Auth0Controller@callback');


Route::group([
    'middleware' => ['auth']
], function () {


    Route::get('/home', 'HomeController@show');


    Route::prefix('{project}')->group(function () {
        Route::get('/projectscope/get/{user_id}', 'Projectscopes@get');
        Route::get('/projectscope/combos', 'Projectscopes@combos');
        Route::resource('/projectscope', 'Projectscopes');
        Route::resource('/projectscopefeedback', 'Projectscopefeedbacks');
    });




});